#!/bin/bash

# 该脚本是为了方便国内下载 k8s 必须镜像而生.
# 安装时, 修改 images 中的版本号即可. 
# 版本号要与 kubeadm config images list 输出的版本号一致!
# 执行: . k8s_base_image.sh 或: source k8s_base_image.sh

images=(
    kube-apiserver:v1.16.2
    kube-controller-manager:v1.16.2
    kube-scheduler:v1.16.2
    kube-proxy:v1.16.2
    pause:3.1
    etcd:3.3.15-0
    coredns:1.6.2
)

for imageName in ${images[@]} ; do
    docker pull registry.cn-hangzhou.aliyuncs.com/google_containers/$imageName
    docker tag registry.cn-hangzhou.aliyuncs.com/google_containers/$imageName k8s.gcr.io/$imageName
done